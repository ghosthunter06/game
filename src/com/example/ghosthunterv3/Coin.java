package com.example.ghosthunterv3;

import com.example.ghosthunterv3.gameWorld.OurView;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

public class Coin {

	int s, h;

	int sSpeed, hSpeed;

	int height, width;

	Bitmap money;

	OurView ov;

	int currentFrame = 0;

	int direction = 0;

	public Coin(OurView ourView, Bitmap money) {

		s = (int) (Math.random() * 1000);

		h = 0;

		sSpeed = 0;

		hSpeed = 3;

		this.money = money;

		ov = ourView;

		height = money.getHeight() / 8;

		width = money.getWidth() / 8;

	}

	public void drawCoin(Canvas canvas) {

		update();

		int srcA = currentFrame * height;

		Rect src = new Rect(0, srcA, width, srcA + height);

		Rect dst = new Rect(s, h, s + width, h + height);

		canvas.drawBitmap(money, src, dst, null);

	}

	private void update() {

		try {

			Thread.sleep(30);

		} catch (InterruptedException e) {

			e.printStackTrace();

		}

		currentFrame = ++currentFrame % 8;

		h += hSpeed;
	}
}