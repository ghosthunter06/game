package com.example.ghosthunterv3;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;

public class gameWorld extends Activity implements OnTouchListener {

	MediaPlayer gameMusic;

	Bitmap ghost;

	OurView v;

	Bitmap blob;

	float x;

	float y;

	float a, b;

	float aSpeed, bSpeed;

	Sprite sprite;

	Rect mainCharacterBox;

	Bitmap killButton;

	Rect killButtonBox;
	
	int ghostsKilled;

	private SharedPreferences mPrefs;
	
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		gameMusic = MediaPlayer.create(gameWorld.this, R.raw.gamemusic);

		gameMusic.start();

		gameMusic.setLooping(true);

		ghost = BitmapFactory.decodeResource(getResources(),R.drawable.ghostenemy);

		v = new OurView(this);

		v.setOnTouchListener(this);

		x = y = 0;

		setContentView(v);

		blob = BitmapFactory.decodeResource(getResources(),R.drawable.femalechara1);

		mainCharacterBox = new Rect((int) x, (int) y, (int) x + 32,(int) y + 32); // rectangle to represent character hit box

		SharedPreferences mPrefs = getSharedPreferences("save", 0);
        ghostsKilled = mPrefs.getInt("kills", 0);
	}

	@Override
	protected void onPause() {

		super.onPause();

		gameMusic.pause();

		v.pause();

		
		SharedPreferences.Editor ed = getSharedPreferences("save", 0).edit();
        ed.putInt("kills", ghostsKilled);
        ed.commit();
	}

	@Override
	protected void onResume() {

		super.onResume();

		gameMusic.start();

		v.resume();

	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

	public class OurView extends SurfaceView implements Runnable {

		Thread t = null;

		SurfaceHolder holder;

		boolean isItOk = false;

		Bitmap w;

		Bitmap r;
		
		Bitmap b;

		int money = 0;
		
		 String moneyString = "Money: " + money;
		// Rect repellentBox;

		int spawnTimer;

		Rect proximityBox;
		
		Rect playerHitBox;

		Paint paint = new Paint();
		
		Bomb bomb;

		private ArrayList<Sprite> sprites = new ArrayList<Sprite>();
		
		private ArrayList<Bomb> bombList = new ArrayList<Bomb>();

		public OurView(Context context) {

			super(context);
			holder = getHolder();
			// + money;

			sprites = new ArrayList<Sprite>();
			
			bombList = new ArrayList<Bomb>();

			// Coin coin=new Coin(this,money);

			w = BitmapFactory.decodeResource(getResources(), R.drawable.warning);

			r = BitmapFactory.decodeResource(getResources(),R.drawable.repellent);
			
			b = BitmapFactory.decodeResource(getResources(),R.drawable.bomb);
			
			

		}

		@Override
		public void run() {

			while (isItOk == true) {

				if (!holder.getSurface().isValid()) {

					continue;

				}

					sprite = new Sprite(OurView.this, ghost, mainCharacterBox);		
					
					bomb = new Bomb (OurView.this, b);
					
				if (money > 9) {

				}

				proximityBox = new Rect((int) x - 150, (int) y - 150,(int) x + 150, (int) y + 150);
				
				playerHitBox = new Rect ((int) x, (int) y, (int) x + 32, (int) y + 32);

				Canvas c = holder.lockCanvas();

				onDraw(c);

				holder.unlockCanvasAndPost(c);

				if (spawnTimer % 100 == 0) {
					sprites.add(createSprite(R.drawable.ghostenemy));
				}
				
				if (spawnTimer % 100 == 0) {
					bombList.add(createBomb(R.drawable.bomb));
				}
				spawnTimer += 2;
				
				
				
			}

		}

		private Sprite createSprite(int resource) {
			Bitmap ghostGroup = BitmapFactory.decodeResource(getResources(),resource);
			return new Sprite(this, ghostGroup, mainCharacterBox);
		}
		
		private Bomb createBomb(int resource){
			Bitmap bombGroup = BitmapFactory.decodeResource(getResources(),resource);
			return new Bomb(this, bombGroup);
		}

		protected void onDraw(Canvas c) {

			paint.setColor(Color.WHITE);
			paint.setTextSize(50);

			c.drawARGB(255, 255, 255, 255);

			c.drawColor(Color.BLACK);

			c.drawBitmap(blob, x, y, null);

			c.drawText("Money: " + money, 500, 50, paint);

			c.drawText("Kills: " + ghostsKilled, 750, 50, paint);
			
			for (Bomb bomb : bombList){
				bomb.drawBomb(c);
			}

			for (Sprite sprite : sprites) {
				sprite.drawGhost(c, x, y);
			}

			// coin.drawCoin(c);

			for (Sprite sprite : sprites){
			if (proximityBox.contains(sprite.getSpriteXLocation(),sprite.getSpriteYLocation())) {
				c.drawBitmap(w, sprite.getSpriteXLocation() + 60,sprite.getSpriteYLocation(), null);
			}
			}
			
			for (int i = 0; i< sprites.size(); i++) {
				if (playerHitBox.intersect(sprites.get(i).spriteRect)) {
					sprites.remove(i);
					ghostsKilled++;
				}

			}
			
			for (int f = 0; f< bombList.size(); f++){
			if (playerHitBox.intersect(bombList.get(f).getBombRect())){
				ghostsKilled += sprites.size();
				sprites.clear();
				bombList.remove(f);
			}
			}
		}

		public void pause() {

			isItOk = false;

			while (true) {

				try {

					t.join();

				} catch (InterruptedException e) {

					e.printStackTrace();

				}

				break;

			}

			t = null;

		}

		public void resume() {

			isItOk = true;

			t = new Thread(this);

			t.start();

		}

	}

	@Override
	public boolean onTouch(View v, MotionEvent me) {

		try {

			Thread.sleep(50);

		} catch (InterruptedException e) {

			e.printStackTrace();

		}

		switch (me.getAction()) {

		case MotionEvent.ACTION_MOVE:

			x = me.getX();

			y = me.getY();

			break;

		}

		return true;

	}

}